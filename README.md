# Image overlay

ROS node that overlays an image read from a file to an Image topic.

The node expects the overlay image (image file) to have the same size as the background image (image topic), 
if the dimensions are different the overlay image will be resized.

## How to launch

using roslaunch:

`roslaunch ros_image_overlay image_overlay.launch input_topic:=/image_topic overlay_image_path:=/path/to/image.png`

or using rosrun

`rosrun ros_image_overlay image_overlay.launch _input_topic:=/image_topic _overlay_image_path:=/path/to/image.png`

## Parameters

- `input_topic` : ROS Image Topic to subscribe.
- `overlay_image_path` : Path to an Image file, that will be used as overlay.

## Outputs

The image with overlay will be published in the topic: `image_overlay`

---

Files to reproduce the single-shot experiments are included inside the `overlay` directory:

|Experiment|Image|
|---|---|
|6 Checkerboards `6_delta_1.png`|![delta_1](./overlay/6_delta_1.png)|
|6 Checkerboards `6_delta_c.png`|![delta_1](./overlay/6_delta_c.png)|
|7 Checkerboards `7_epsilon_1.png`|![delta_1](overlay/7_epsilon_1.png)|
|7 Checkerboards `7_epsilon_c.png`|![delta_1](overlay/7_epsilon_c.png)|
